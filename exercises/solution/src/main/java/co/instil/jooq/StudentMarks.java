package co.instil.jooq;

/**
 * Pojo representation of student marks.
 */
public class StudentMarks {
    private String firstName;
    private String lastName;
    private String title;
    private Double overallMark;
    private Integer rank;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getOverallMark() {
        return overallMark;
    }

    public void setOverallMark(Double overallMark) {
        this.overallMark = overallMark;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String rankSummary() {
        return String.format(" %-10s | %-10s | %-10s | %g | %2d   | %n", title, firstName, lastName, overallMark, rank);
    }

    public static String summaryHeader() {
        return "\n\n" +
                "+-----------+------------+------------+---------+------+\n" +
                "+ Subject   + First name + Last name  + Overall + Rank +\n" +
                "+-----------+------------+------------+---------+------+\n";


    }
}

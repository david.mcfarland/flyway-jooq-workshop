package co.instil.jooq;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Entry point into flyway application.
 */
public class JOOQApplication {
    public static void main(String... args) {
        String url = args[0];
        String user = args[1];
        String password = args[2];

        Connection conn = null;
        try {
            Class.forName("org.postgresql.Driver").newInstance();
            conn = DriverManager.getConnection(url, user, password);

            DSLContext create = DSL.using(conn, SQLDialect.POSTGRES);
            SchoolService school = new SchoolService(create);

            school.cleanOldRecords();
            school.populateWithStudents();
            school.populateWithSubjects();
            school.populateWithResults();
            school.printStudentsMarks();

            school.printStudentRank();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ignore) {
                }
            }
        }


    }
}

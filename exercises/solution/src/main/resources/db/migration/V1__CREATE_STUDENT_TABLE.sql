CREATE TABLE STUDENT (
  STUDENT_ID    UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  FIRST_NAME    VARCHAR(30) NOT NULL,
  LAST_NAME     VARCHAR(30) NOT NULL,
  DATE_OF_BIRTH TIMESTAMP
);
package co.instil.jooq;

import com.google.common.collect.Lists;
import org.joda.time.DateTime;
import org.jooq.DSLContext;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Random;

import static org.jooq.instil.tables.Results.RESULTS;
import static org.jooq.instil.tables.Student.STUDENT;
import static org.jooq.instil.tables.Subject.SUBJECT;

/**
 * School service.
 */
public class SchoolService {
    private final DSLContext sql;


    public SchoolService(DSLContext create) {
        this.sql = create;
    }

    public void cleanOldRecords() {
        sql.delete(STUDENT).execute();
        sql.delete(SUBJECT).execute();
        sql.delete(RESULTS).execute();
    }

    public void populateWithStudents() {
        for (int i = 0; i < 10; i++) {
            sql
                    .insertInto(STUDENT, STUDENT.FIRST_NAME, STUDENT.LAST_NAME, STUDENT.DATE_OF_BIRTH)
                    .values(random(firstName), random(lastName), randomDob())
                    .execute();
        }
    }

    public void populateWithSubjects() {
        for (String subject : subjects) {
            // Insert all subjects into the database. See populateStudent method for example.
        }
    }

    public void populateWithResults() {
        // Hint look at DAO object to collect current student and subject IDs.
        // Insert random marks - note that you can't put a coursework mark into a exam mark and vica versa.
    }

    public void printStudentsMarks() {
        // Print table with the following format - see http://www.jooq.org/javadoc/3.4.x/org/jooq/Result.html#format--.
        // TITLE | FIRST_NAME | STUDENT.LAST_NAME | RESULTS.COURSEWORK_MARK | RESULTS.EXAM_MARK
    }

    /**
     * Helpers for generating random data to insert into database.
     */
    private Random random = new Random();
    private DateTime now = new DateTime();
    private List<String> firstName = Lists.newArrayList("Danna", "Katharine", "Ellie", "Rueben", "Rosaria", "Rigoberto", "Christa", "Samuel", "Vicky", "Yahaira");
    private List<String> lastName = Lists.newArrayList("Donnelly", "Climer", "Wiedemann", "Mcbean", "Mcelfresh", "Moyle", "Talbott", "Finnell", "Juntunen", "Jaco");
    private List<String> subjects = Lists.newArrayList("Maths", "Chemistry", "Biology", "English", "Computing");


    private <T> T random(List<T> targetList) {
        return targetList.get(random.nextInt(targetList.size()));
    }


    private Timestamp randomDob() {
        return new Timestamp(now.minusDays(16 * 265 + random.nextInt(10000)).getMillis());
    }

    private Integer randomExamMark() {
        return random.nextInt(100);
    }

    private BigDecimal randomCourseworkMark() {
        return BigDecimal.valueOf(random.nextDouble() * 100);
    }
}

package co.instil.flyway;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.MigrationInfoService;

/**
 * Entry point into flyway application.
 */
public class FlywayApplication {
    private final static String formatMask = "| %-7s | %-30s | %-10s |%n";
    private final static String separator = "+---------+--------------------------------+------------+%n";

    public static void main(String... args) {
        String url = args[0];
        String user = args[1];
        String password = args[2];

        Flyway flyway = new Flyway();
        flyway.setDataSource(url, user, password);
        printMigrationInfo(flyway.info());
    }

    private static void printMigrationInfo(MigrationInfoService info) {
        printHeader();
        for (MigrationInfo migrationInfo : info.all()) {
            print(migrationInfo);
        }
        printFooter();
    }

    private static void printHeader() {
        System.out.format(separator);
        System.out.format(formatMask, "VERSION", "DESCRIPTION", "STATE");
        System.out.format(separator);
    }

    private static void printFooter() {
        System.out.format(separator);
    }

    private static void print(MigrationInfo migrationInfo) {
        System.out.format(formatMask, migrationInfo.getVersion(), migrationInfo.getDescription(), migrationInfo.getState());
    }
}

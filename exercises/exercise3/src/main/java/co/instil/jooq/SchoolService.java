package co.instil.jooq;

import com.google.common.collect.Lists;
import org.joda.time.DateTime;
import org.jooq.DSLContext;
import org.jooq.instil.enums.QualificationTypes;
import org.jooq.instil.tables.daos.StudentDao;
import org.jooq.instil.tables.daos.SubjectDao;
import org.jooq.instil.tables.pojos.Student;
import org.jooq.instil.tables.pojos.Subject;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Random;

import static org.jooq.instil.tables.Results.RESULTS;
import static org.jooq.instil.tables.Student.STUDENT;
import static org.jooq.instil.tables.Subject.SUBJECT;

/**
 * School service.
 */
public class SchoolService {
    public static final String OVERALL_MARK = "overall_mark";
    private final DSLContext sql;


    public SchoolService(DSLContext create) {
        this.sql = create;
    }

    public void cleanOldRecords() {
        sql.delete(STUDENT).execute();
        sql.delete(SUBJECT).execute();
        sql.delete(RESULTS).execute();
    }

    public void populateWithStudents() {
        for (int i = 0; i < 10; i++) {
            sql
                    .insertInto(STUDENT, STUDENT.FIRST_NAME, STUDENT.LAST_NAME, STUDENT.DATE_OF_BIRTH)
                    .values(random(firstName), random(lastName), randomDob())
                    .execute();
        }
    }

    public void populateWithSubjects() {
        for (String subject : subjects) {
            sql
                    .insertInto(SUBJECT, SUBJECT.TITLE, SUBJECT.QUALIFICATION_TYPE)
                    .values(subject, random(Lists.newArrayList(QualificationTypes.values())))
                    .execute();
        }
    }

    public void populateWithResults() {
        SubjectDao subjectDao = new SubjectDao(sql.configuration());
        for (Subject subject : subjectDao.findAll()) {
            StudentDao studentsDao = new StudentDao(sql.configuration());
            for (Student student : studentsDao.findAll()) {
                insertMarkFor(subject, student);
            }
        }
    }

    private void insertMarkFor(Subject subject, Student student) {
        sql.insertInto(RESULTS, RESULTS.STUDENT_ID, RESULTS.SUBJECT_ID, RESULTS.EXAM_MARK, RESULTS.COURSEWORK_MARK, RESULTS.DATE)
                .values(student.getStudentId(), subject.getSubjectId(), randomExamMark(), randomCourseworkMark(), randomDob())
                .execute();
    }

    public void printStudentsMarks() {
        System.out.println(sql
                .select(SUBJECT.TITLE, STUDENT.FIRST_NAME, STUDENT.LAST_NAME, RESULTS.COURSEWORK_MARK, RESULTS.EXAM_MARK)
                .from(STUDENT)
                .join(RESULTS)
                .on(RESULTS.STUDENT_ID.eq(STUDENT.STUDENT_ID))
                .join(SUBJECT)
                .on(RESULTS.SUBJECT_ID.eq(SUBJECT.SUBJECT_ID))
                .orderBy(SUBJECT.TITLE, STUDENT.LAST_NAME, STUDENT.FIRST_NAME)
                .fetch()
                .format());
    }

    public void printStudentRank() {
        // Fetch overall mark and exam rank by subject into StudentMarks list.
        // Coursework makes up 40% of overall mark and the exam 60%. 
        // See window functions rank http://www.postgresql.org/docs/9.3/static/tutorial-window.html
        List<StudentMarks> summary = null;

        System.out.print(StudentMarks.summaryHeader());
        for (StudentMarks marks : summary) {
            System.out.print(marks.rankSummary());
        }
    }

    /**
     * Helpers for generating random data to insert into database.
     */
    private Random random = new Random();
    private DateTime now = new DateTime();
    private List<String> firstName = Lists.newArrayList("Danna", "Katharine", "Ellie", "Rueben", "Rosaria", "Rigoberto", "Christa", "Samuel", "Vicky", "Yahaira");
    private List<String> lastName = Lists.newArrayList("Donnelly", "Climer", "Wiedemann", "Mcbean", "Mcelfresh", "Moyle", "Talbott", "Finnell", "Juntunen", "Jaco");
    private List<String> subjects = Lists.newArrayList("Maths", "Chemistry", "Biology", "English", "Computing");


    private <T> T random(List<T> targetList) {
        return targetList.get(random.nextInt(targetList.size()));
    }


    private Timestamp randomDob() {
        return new Timestamp(now.minusDays(16 * 265 + random.nextInt(10000)).getMillis());
    }

    private Integer randomExamMark() {
        return random.nextInt(100);
    }

    private BigDecimal randomCourseworkMark() {
        return BigDecimal.valueOf(random.nextDouble() * 100);
    }


}

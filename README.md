# A quick workshop introducing [Flyway](http://flywaydb.org/) and [jOOQ](http://www.jooq.org/).

## Exercise 1
* Experiment with Flyway and see how it handles versioning and rollback.

## Exercise 2 
* Experiment with jOOQ and observe how it enforces type safety and provides a good DSL to interact with RDBMS.

## Exercise 3
* Look at the more advanced features of SQL which are exposed via jOOQ which are not availble via ORM's.